#!/usr/bin/env groovy
// Maintainer: laura.santamaria@rackspace.com

// requires JenkinsPipelineUnit plugin. See https://github.com/jenkinsci/JenkinsPipelineUnit

import groovy.lang.*
import java.io.*
import org.junit.*
import com.lesfurets.jenkins.unit.BasePipelineTest

class JenkinsfileJob extends BasePipelineTest {

    // Initial overrides
    @Override
    @Before
    void setUp() throws Exception {
        // Define some base configuration for the repo layout
        baseScriptRoot = ''
        scriptRoots += 'src/main/groovy'
        scriptRoots += ''
        scriptExtension = 'groovy'
        super.setUp()
        def scmBranch = 'pr-123'
        // Assign job parameters and environment variables
        binding.setVariable('JENKINS_HOME', 'build')
        binding.setVariable('env', ['BUILD_TAG': 'testing', 'BUILD_URL': 'string'])
        binding.setVariable('envelopeDir', 'string')
        binding.setVariable('assetDir', 'string')
        binding.setVariable('contentIDBase', 'string')
        binding.setVariable('contentRoot', 'string')
        binding.setVariable('contentServiceURL', 'string')
        binding.setVariable('contentServiceAPIKey', '123')
        binding.setVariable('BRANCH_NAME', scmBranch)
        binding.setVariable('MY_CREDS', 'string')
        // Explain what pwd, sh, scm, ws, deleteDir mean
        helper.registerAllowedMethod('pwd', [], { 'src/main/groovy' })
        helper.registerAllowedMethod('sh', [Map.class], {c -> 'bcc19744'})
        binding.setVariable('scm', [
                        $class                          : 'GitSCM',
                        branches                        : [[name: scmBranch]]
        ])
        helper.registerAllowedMethod('ws', [String.class, Closure.class], { String path, Closure body ->
            body()
        })
        helper.registerAllowedMethod('deleteDir', [], null)
        helper.registerAllowedMethod('usernameColonPassword', [Map.class], { Map map ->
            result1 = map.findAll { key, value -> key.contains("credentialsId") }.values()
            result2 = map.findAll { key, value -> key.contains("variable") }.values()
            return "${result1}:${result2}"
        })
    }

    // Tests

    @Test
    void should_execute_without_errors() throws Exception {
        println System.getProperty('user.dir')
        runScript("ContentJenkinsfile")
        printCallStack()
    }

    @Test
    void should_execute_without_errors() throws Exception {
        println System.getProperty('user.dir')
        runScript("ControlJenkinsfile")
        printCallStack()
    }

    // @Test
    // void should_execute_without_errors() throws Exception {
    //     loadScript("Jenkinsfile1")
    //     // assertThat(helper.callStack.findAll { call ->
    //     //     call.methodName == "sh"
    //     // }.any { call ->
    //     //     callArgsToString(call).contains("mvn verify")
    //     // }).isTrue()
    //     assertJobStatusSuccess()
    // }

    // @Test
    // void testNonReg() throws Exception {
    //     def script = loadScript("Jenkinsfile")
    //     script.execute()
    //     super.testNonRegression('example')
    // }


}
